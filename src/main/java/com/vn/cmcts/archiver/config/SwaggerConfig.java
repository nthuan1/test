package com.vn.cmcts.archiver.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author hendisantika
 * @date 4/24/17
 */

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    private static final String BASE_PACKAGE = "com.vn.cmcts.archiver.controller";
    private static final String BASE_URL = "localhost";
    private static final String TITLE = "Vietlot HRM";
    private static final String DESCRIPTION = "Vietlot Human Resource Management - REST API";
    private static final String VERSION = "1.0.0";

    @Bean
    public Docket api() {

//        ParameterBuilder aParameterBuilder = new ParameterBuilder();
//        aParameterBuilder.name("Authorization")
//                .modelRef(new ModelRef("string"))
//                .parameterType("header")
//                .defaultValue("Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwMDAxMSIsImlhdCI6MTYyMjU1OTc2MCwic3ViIjoiMDAwMTEiLCJpc3MiOiIwMDAxMSIsImV4cCI6MTYyMzQyMzc2MH0.bZ4Mo2JPkln2Lb61cC-7HsJzrZxM8RjC39p3kJMCsy0")
//                .required(false)
//                .build();
//        List<Parameter> aParameters = new ArrayList<>();
//        aParameters.add(aParameterBuilder.build());

        return new Docket(DocumentationType.SWAGGER_2)//
                .select()//
                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
                //.apis(RequestHandlerSelectors.any())//
                .paths(PathSelectors.any())//
                .build()//
                .apiInfo(metadata())//
                .useDefaultResponseMessages(false)//
                .securitySchemes(Collections.singletonList(apiKey()))
                .securityContexts(Collections.singletonList(securityContext()))
//                .tags(new Tag("users", "Operations about users"))//
                //.globalOperationParameters(aParameters)
                .genericModelSubstitutes(Optional.class);

    }

    private ApiInfo metadata() {
        return new ApiInfoBuilder()//
                .title(TITLE)//
                .description(DESCRIPTION)//
                .version("1.0.0")//
                .license("MIT License").licenseUrl("http://opensource.org/licenses/MIT")//
                .contact(new Contact(null, null, "dev@cmc.com.vn"))//
                .build();
    }

    private ApiKey apiKey() {
        return new ApiKey("Authorization", "Authorization", "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.any())
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList(new SecurityReference("Authorization", authorizationScopes));
    }

//    @Bean
//    public Docket foodsApi() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                //.host(BASE_URL)
//                .pathProvider(new ExtendRelativePathProvider())
//                .select()
//                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
//                .build()
//                //.ignoredParameterTypes(Usuario.class)
//                .globalOperationParameters(
//                        Arrays.asList(
//                                new ParameterBuilder()
//                                        .name("Authorization")
//                                        .description("Header para Token JWT")
//                                        .modelRef(new ModelRef("string"))
//                                        .parameterType("header")
//                                        .required(false)
//                                        .build()))
//                .apiInfo(metaData());
//    }
//
//    private ApiInfo metaData() {
//        return new ApiInfoBuilder()
//                .title(TITLE)
//                .description(DESCRIPTION)
//                .version(VERSION)
//                .build();
//    }
//
////    @Override
////    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
////        registry.addResourceHandler("swagger-ui.html")
////                .addResourceLocations("classpath:/META-INF/resources/");
////
////        registry.addResourceHandler("/webjars/**")
////                .addResourceLocations("classpath:/META-INF/resources/webjars/");
////    }
//
//    public class ExtendRelativePathProvider extends AbstractPathProvider{
//
//        public static final String ROOT = "/";
//        private static final String BASE_PATH = "/";
//
//        @Override
//        protected String applicationPath() {
//            String contextPath = BASE_PATH;
//            return contextPath == null ? ROOT : contextPath;
//        }
//        @Override
//        protected String getDocumentationPath() {
//            return BASE_PATH;
//        }
//    }
}
