package com.vn.cmcts.archiver.model;

public enum RoleName {
	ROLE_USER,
    ROLE_THERAPIST,
    ROLE_ADMIN
}
