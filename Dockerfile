# Start with a base image containing Java runtime https://hub.docker.com/r/adoptopenjdk/openjdk11
FROM maven:3.6-jdk-11 as maven_build
#Possibility to set JVM options (https://www.oracle.com/technetwork/java/javase/tech/vmoptions-jsp-140102.html)
#Set app home folder
ENV APP_HOME=/home/root/build/
WORKDIR $APP_HOME
COPY . .
RUN mvn clean package -Dmaven.test.skip

# Start with a base image containing Java runtime
FROM openjdk:15-slim as runtime
WORKDIR /home/root/user/
COPY --from=maven_build /home/root/build/target/d-archiver-0.1.0-SNAPSHOT.jar $WORKDIR
EXPOSE 8080
ENTRYPOINT [ "sh", "-c", "java -jar d-archiver-0.1.0-SNAPSHOT.jar" ]